package pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class Base64ToFilePage {

    public final static String URL_BASE_64_TO_FILE_PAGE = "https://base64.guru/converter/decode/file";
    private final SelenideElement base64TextField = $("textarea[name='base64']");
    private final SelenideElement decodeBase64ToFileButton = $("button[name='decode']");
    private final SelenideElement downloadTxtLink = $("a[data-field-id='form-base64-converter-decode-file-preview']");


    @Step("Open the link: " + URL_BASE_64_TO_FILE_PAGE)
    public Base64ToFilePage openPage() {
        open(URL_BASE_64_TO_FILE_PAGE);
        return this;
    }

    @Step("Fill the text field with '{text}'")
    public Base64ToFilePage fillTextField(String text) {
        base64TextField.setValue(text);
        return this;
    }

    @Step("Click to the 'Decode Base64 to File' button")
    public Base64ToFilePage clickDecodeBase64ToFileButton() {
        decodeBase64ToFileButton.click();
        return this;
    }

    @Step("Click to the downloadable link")
    public File clickDownloadFile() {
        return downloadTxtLink.scrollTo()
                .download();
    }
}
