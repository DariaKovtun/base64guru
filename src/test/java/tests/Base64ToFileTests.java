package tests;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import pages.Base64ToFilePage;
import utils.AllureUtils;
import utils.FileUtils;
import utils.TestExecutionStatus;
import utils.TestExecutionStatusResolver;
import utils.TestListener;

import java.io.File;
import java.io.IOException;

@ExtendWith(TestExecutionStatusResolver.class)
public class Base64ToFileTests {

    @ExtendWith({TestListener.class})
    @ParameterizedTest(name = "[{0}, {1}, {2}]")
    @CsvSource({"'SGVsbG8sIFdvcmxkIQ==', 'Hello, World!', 'txt'"})
    @DisplayName("Decode Base64 to file, verify content and extension")
    public void downloadAndVerifyFileTest(String base64, String text, String extension) throws IOException {
        Base64ToFilePage page = new Base64ToFilePage();
        File file = page.openPage()
                .fillTextField(base64)
                .clickDecodeBase64ToFileButton()
                .clickDownloadFile();
        String fileContent = FileUtils.readFileContent(file);
        String fileExtension = FileUtils.getExtension(file);
        Assertions.assertEquals(text, fileContent,
                String.format("File content mismatch: expected [%s], but found [%s]", text, fileContent));
        Assertions.assertEquals(extension, fileExtension,
                String.format("File extension mismatch: expected [%s], but found [%s]", extension, fileExtension));
    }

    @AfterEach
    public void cleanUp(TestExecutionStatus status) throws IOException {
        if (status == TestExecutionStatus.SUCCESSFUL) {
            FileUtils.deleteFolderRecursively(FileUtils.getNewestSubfolderInFolder());
        }
    }

    @AfterAll
    public static void saveEnvironmentProperties() throws IOException {
        AllureUtils.saveEnvironmentProperties();
    }
}
