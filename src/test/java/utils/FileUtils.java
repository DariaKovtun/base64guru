package utils;

import com.codeborne.selenide.Configuration;
import io.qameta.allure.Attachment;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Comparator;
import java.util.stream.Stream;

public class FileUtils {
    public static final String PATH_FOLDER = Configuration.downloadsFolder;

    @Attachment(value = "Downloaded file", type = "text/plain")
    public static String readFileContent(File file) throws IOException {
        return Files.readString(Path.of(file.getPath()));
    }

    public static String getExtension(File file) {
        String fileName = file.getName();
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    public static Path getNewestSubfolderInFolder() throws IOException {
        try (var walk = Files.walk(Paths.get(PATH_FOLDER))) {
            return walk.filter(Files::isDirectory)
                    .filter(p -> !p.equals(Paths.get(PATH_FOLDER)))
                    .max(Comparator.comparing(FileUtils::getCreationTime))
                    .orElseThrow(() -> new IOException("No directories found"))
                    .toAbsolutePath();
        }
    }

    private static FileTime getCreationTime(Path file) {
        try {
            return Files.readAttributes(file, BasicFileAttributes.class).creationTime();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static void deleteFolderRecursively(Path folder) {
        try {
            if (Files.isDirectory(folder)) {
                try (Stream<Path> entries = Files.list(folder)) {
                    entries.forEach(FileUtils::deleteFolderRecursively);
                }
            }
            Files.delete(folder);
        } catch (IOException e) {
            System.out.println("Folder was not deleted due to: " + e.getMessage());
        }
    }
}
