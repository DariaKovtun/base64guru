package utils;

import com.codeborne.selenide.Configuration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import static pages.Base64ToFilePage.URL_BASE_64_TO_FILE_PAGE;

public class AllureUtils {
    public static void saveEnvironmentProperties() throws IOException {
        Properties properties = new Properties();
        properties.setProperty("browser", Configuration.browser);
        properties.setProperty("application_url", URL_BASE_64_TO_FILE_PAGE);
        File allureResultsDir = new File("allure-results");
        if (!allureResultsDir.exists()) {
            allureResultsDir.mkdirs();
        }
        try (FileOutputStream fos = new FileOutputStream(new File(allureResultsDir, "environment.properties"))) {
            properties.store(fos, "Environment Properties");
        }
    }
}
