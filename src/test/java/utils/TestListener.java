package utils;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class TestListener implements AfterTestExecutionCallback {
    public static final ExtensionContext.Namespace NAMESPACE =
            ExtensionContext.Namespace.create("utils", "TestListener");
    public static final Object TEST_RESULT = new Object();

    @Override
    public void afterTestExecution(ExtensionContext context) {
        context.getExecutionException().ifPresentOrElse(
                throwable -> context.getStore(NAMESPACE).put(TEST_RESULT, TestExecutionStatus.FAILED),
                () -> context.getStore(NAMESPACE).put(TEST_RESULT, TestExecutionStatus.SUCCESSFUL)
        );
    }
}
