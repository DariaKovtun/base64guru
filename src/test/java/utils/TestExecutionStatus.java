package utils;

public enum TestExecutionStatus {
    SUCCESSFUL,
    FAILED
}
