
## Installation
### Quick installation

Quick installation using IDEs by Jetbrains as [IntelliJ IDEA](https://www.jetbrains.com/idea/download/) or [Aqua](https://www.jetbrains.com/aqua/download/):

* Copy the link: `https://gitlab.com/DariaKovtun/base64guru.git`
* In IDE go to `File` -> `New` -> `Project from Version Control`. Paste the URL, then click `Clone`.
* Open the project and follow the IDE suggestions for downloading JDK and Maven, if not installed.
* In the right menu choose `Maven` -> `Reload All Maven Projects`.

### Standard installation
In order to utilise this project you need to have the following installed locally:
    
* [Java 21](https://www.oracle.com/java/technologies/downloads/)
* [Maven 3.9.6](https://maven.apache.org/download.cgi)
* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

After installation run commands:

```
# Clone this repository
$ git clone https://gitlab.com/DariaKovtun/base64guru.git

# Go into the repository
$ cd base64guru 

# Install dependencies
$ mvn clean compile
```  


## Running Tests
### Quick running

* Open the project in Jetbrains IDE
* Go to the `src/test/java/tests` directory in the Project view
* Choose the desired test suite file you wish to run
* Use the shortcut `⌃⇧R (macOS) / Ctrl+Shift+F10 (Windows/Linux)` to run the test suite

### Standard running
To run all tests, run command in the project folder:

```
$ mvn compile test
```
## Opening Allure report

For viewing reports [Allure](https://allurereport.org/docs/gettingstarted-installation/) needs to be installed on the machine. 

To generate report follow this instructions: 
```
$ allure generate --clean  
$ allure open
```

Tests should be run at least once, otherwise the report will be empty.